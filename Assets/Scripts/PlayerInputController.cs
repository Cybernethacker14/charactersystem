﻿using UnityEngine;

public class PlayerInputController : MonoBehaviour
{

    [System.Serializable]
    struct EssentialComponents
    {
        public Animator animator;
    }

    struct AnimationLayerReferences
    {
        public int baseLayerReference;
        public int walkLayerReference;
    }

    [SerializeField]
    EssentialComponents _essentialComponentsReference;

    AnimationLayerReferences _animationLayerReferences;

    Quaternion newrotation;

    [SerializeField]
    public Transform _camera;

    public float debugInputRotation;
    public float playerFacingTransform;

    public float speed;

    public float v = 0.0f;
    public float h = 0.0f;

    float _smooth = 0.05f;

    public bool turning;

    private void Start()
    {
        _essentialComponentsReference.animator.SetLayerWeight(0, 1);
    }

    private void FixedUpdate()
    {
        v = Input.GetAxis("Vertical");
        h = Input.GetAxis("Horizontal");

        Rotate(v, h);
        
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {

            /*
             * allowMoving is controlled by Rotate function
             */
            if(allowMoving)
                transform.position += transform.forward * speed * Time.deltaTime;

            _essentialComponentsReference.animator.SetBool("isWalking", true);
            _essentialComponentsReference.animator.SetBool("isIdle", false);
        }
        else
        {
            _essentialComponentsReference.animator.SetBool("isWalking", false);
            _essentialComponentsReference.animator.SetBool("isIdle", true);
        }
    }

    // Update is called once per frame
    private void Movement()
    {
        debugInputRotation = Mathf.Rad2Deg * Mathf.Atan2(h, v);
        playerFacingTransform = transform.rotation.eulerAngles.y;

        //print(debugInputRotation + ", " + playerFacingTransform);
    }


    private Quaternion rotFrom;
    private float rotVal = 1.1f;
    private bool allowMoving;

    void Rotate(float v, float h)
    {
        /*
         * if the rotation is not happening, and controlls are used, then update new rotation
         * 
         * if the rotation is big enough (30), 
         * use interpolation
         * else update it directly
         * 
         * while interpolation, lock the movement by making `allowMoving` false 
         * and don't allow changing the target rotation till the rotation is complete by keeping 
         * 
         */
        if (rotVal > 1 && (v != 0 || h != 0)) {
            newrotation = Quaternion.Euler(0, _camera.eulerAngles.y + (Mathf.Atan2(h, v) * Mathf.Rad2Deg), 0);

            if (Mathf.Abs(Mathf.DeltaAngle(transform.rotation.eulerAngles.y, newrotation.eulerAngles.y)) > 15)
            {
                rotFrom = transform.rotation;
                allowMoving = false;
                rotVal = 0;
            }
            else
            {
                transform.rotation = newrotation;
                allowMoving = true;
            }

        }

        rotVal += Time.deltaTime * 2; 
        if (rotVal <= 1)
            transform.rotation = Quaternion.Slerp(rotFrom, newrotation, rotVal);

    }
}